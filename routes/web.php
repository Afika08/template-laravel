<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('index');
});

Route::get('/form', function () {
    return view('form');
});

Route::get('/post', function () {
    return view('Hm.wwwelcome');
});

Route::get('/master', function () {
    return view('Admin.master');
});

Route::get('/', function () {
    return view('task.template');
});

Route::get('/data-tables', function () {
    return view('task.tables');
});

Route::get('form', 'FormController@form');
Route::post('post', 'FormController@post');
Route::get('/items', function () {
    return view('items.index');
});

